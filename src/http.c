#define _POSIX_C_SOURCE 200112L // getaddrinfo
#include <errno.h>              // errno
#include <netdb.h>              // addrinfo, getaddrinfo, gai_strerror
#include <stdbool.h>            // bool
#include <stdio.h>              // snprintf, DEBUG_PRINT
#include <stdlib.h>             // malloc
#include <string.h>             // strerror
#include <sys/socket.h>         // socket, connect, SOCK_STREAM, AF_UNSPEC
#include <unistd.h>             // close

#include "http.h"
#include "utils.h"
#include "debug.h"

#define MAX_HEADER_SIZE  1024
#define MAX_HOSTNAME_LEN 256

#define GET_REQ_HEADER    \
    "GET %s HTTP/1.1\r\n" \
    "Host: %s\r\n"        \
    "Accept: %s\r\n"      \
    "\r\n"

#define POST_REQ_HEADER      \
    "POST %s HTTP/1.1\r\n"   \
    "Host: %s\r\n"           \
    "Accept: %s\r\n"         \
    "Content-Length: %u\r\n" \
    "Content-Type: %s\r\n"   \
    "\r\n"

struct http_conn {
    int sockfd;
    char *hostname;
    char *route;
};

static void http_conn_init(struct http_conn **conn, const char *addr, int sockfd);

static bool http_create_get_request_header(char *header, const char *route, const char *host, const char *accept)
{
    int success = false;

    if (!header || !route || !host || !accept) {
        DEBUG_PRINT("http_create_get_request_header: NULL args\n");
        goto exit;
    }

    int bytes_written = snprintf(header, MAX_HEADER_SIZE, GET_REQ_HEADER, route, host, accept);
    if (bytes_written == -1 || bytes_written >= MAX_HEADER_SIZE) {
        DEBUG_PRINT("http_create_get_request_header: snprintf failed\n");
        goto exit;
    }

    success = true;
exit:
    return success;
}

struct http_conn *http_init(const char *addr)
{
    struct http_conn *conn = NULL;

    struct addrinfo hints = {
        .ai_family = AF_UNSPEC,
        .ai_socktype = SOCK_STREAM,
        .ai_flags = 0,
        .ai_protocol = 0,
        .ai_canonname = NULL,
        .ai_addr = NULL,
        .ai_next = NULL
    };
    struct addrinfo *server_info = NULL;

    int result = getaddrinfo(addr, "http", &hints, &server_info);
    if (result != 0) {
        DEBUG_PRINT("getaddrinfo: %s\n", gai_strerror(result));
        goto cleanup;
    }

    int sockfd = -1;
    struct addrinfo *ai = NULL;
    for (ai = server_info; ai != NULL; ai = ai->ai_next) {
        sockfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
        if (sockfd == -1) {
            continue;
        }

        if (connect(sockfd, ai->ai_addr, ai->ai_addrlen) == -1) {
            close(sockfd);
            sockfd = -1;
            continue;
        }

        DEBUG_PRINT("Connected to %s\n", addr);
        break;
    }

    if (ai == NULL) {
        DEBUG_PRINT("Failed to connect to %s\n", addr);
        goto cleanup;
    }

    http_conn_init(&conn, addr, sockfd);
    if (!conn) {
        DEBUG_PRINT("Failed to initialize connection struct\n");
        close(sockfd);
    }

cleanup:
    freeaddrinfo(server_info);
    return conn;
}

static void http_conn_init(struct http_conn **conn, const char *addr, int sockfd)
{
    *conn = malloc(sizeof(**conn));
    if (!*conn) {
        DEBUG_PRINT("Failed to malloc: struct conn\n");
        goto exit;
    }

    size_t addr_len = strlen(addr);
    (*conn)->hostname = malloc(addr_len + 1);
    if (!(*conn)->hostname) {
        DEBUG_PRINT("Failed to malloc: conn->hostname\n");
        goto cleanup_conn;
    }
    strncpy((*conn)->hostname, addr, addr_len);

    (*conn)->route = malloc(sizeof("/"));
    if (!(*conn)->route) {
        DEBUG_PRINT("Failed to malloc: conn->route\n");
        goto cleanup_hostname;
    }
    strcpy((*conn)->route, "/");

    (*conn)->sockfd = sockfd;
    goto exit;

cleanup_hostname:
    free((*conn)->hostname);
    (*conn)->hostname = NULL;
cleanup_conn:
    free(*conn);
    *conn = NULL;
exit:
    return;
}

void http_cleanup(struct http_conn *conn)
{
    close(conn->sockfd);
    free(conn->hostname);
    free(conn->route);
    free(conn);
    conn = NULL;
}

void http_get_request(const struct http_conn *conn)
{
    char headers[MAX_HEADER_SIZE];
    if (!http_create_get_request_header(headers, conn->route, conn->hostname, "*/*")) {
        return;
    }

    DEBUG_PRINT("GET Request Headers:\n");
    DEBUG_PRINT("%s", headers);

    send_data(conn->sockfd, headers, sizeof(headers));
}

void http_get_response(const struct http_conn *conn, void *buf, const size_t buf_len)
{
    recv_data(conn->sockfd, buf, buf_len);
}
