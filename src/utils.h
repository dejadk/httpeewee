#pragma once
#include <stddef.h> // size_t

ssize_t send_data(const int sockfd, const void *data, const size_t data_len);
ssize_t recv_data(const int sockfd, void *buf, const size_t buf_len);
