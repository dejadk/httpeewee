#include <errno.h>      // errno
#include <stdio.h>      // DEBUG_PRINT
#include <string.h>     // strerror
#include <sys/socket.h> // send, recv

#include "debug.h"
#include "utils.h"

ssize_t send_data(const int sockfd, const void *data, const size_t data_len)
{
    ssize_t total_bytes_sent = 0;

    DEBUG_PRINT("Sending data...\n");

    do {
        ssize_t bytes_sent = send(sockfd, data, data_len, 0);
        if (bytes_sent == -1) {
            DEBUG_PRINT("send: %s\n", strerror(errno));
            goto exit;
        }

        total_bytes_sent += bytes_sent;

        DEBUG_PRINT("Sent:  %zd\n", bytes_sent);
        DEBUG_PRINT("Total: %zd\n", total_bytes_sent);
    } while (total_bytes_sent < data_len);

    DEBUG_PRINT("Data finished sending\n");
exit:
    return total_bytes_sent;
}

ssize_t recv_data(const int sockfd, void *buf, const size_t buf_len)
{
    ssize_t bytes_received = 0;

    bytes_received = recv(sockfd, buf, buf_len, 0);
    if (bytes_received == -1) {
        DEBUG_PRINT("recv: %s\n", strerror(errno));
        goto exit;
    } else if (bytes_received == 0) {
        DEBUG_PRINT("Unexpected shutdown while receiving GET response\n");
        goto exit;
    }

    DEBUG_PRINT("Bytes received: %zd\n", bytes_received);
exit:
    return bytes_received;
}
