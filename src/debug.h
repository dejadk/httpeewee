#pragma once
#include <stdio.h>

#ifdef DEBUG
#define DEBUG_PRINT(fmt, ...)                                            \
    do {                                                                 \
        printf("[DEBUG] %s#%d " fmt, __FILE__, __LINE__, ##__VA_ARGS__); \
    } while (0)
#else
#define DEBUG_PRINT(fmt, ...) \
    do {                      \
    } while (0)
#endif
