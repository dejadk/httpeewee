#pragma once
#include <stddef.h> // size_t

struct http_conn;

struct http_conn *http_init(const char *addr);
void http_cleanup(struct http_conn *conn);
void http_get_request(const struct http_conn *conn);
void http_get_response(const struct http_conn *conn, void *buf, const size_t buf_len);
