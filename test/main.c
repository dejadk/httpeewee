#include <stdio.h>  // printf
#include <unistd.h> // close

#include "http.h"

#define RECV_BUF_SIZE 4096

int main(void)
{
    // Fix: A trailing backslash breaks this, e.g. "www.google.com/"
    //      A leading "http://" breaks this, e.g. "http://www.google.com"
    //      "www.google.com" works
    //      "google.com" works
    struct http_conn *conn = http_init("www.example.com");
    if (!conn) {
        printf("Conn is null\n");
        return 1;
    }

    http_get_request(conn);

    char buf[RECV_BUF_SIZE];
    http_get_response(conn, buf, sizeof(buf));

    printf("GET Response:\n%s\n", buf);
    http_cleanup(conn);
}
